terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.31.0"
    }
}
backend "http" {}
}

# Configure and downloading plugins for aws
provider "aws" {
  region     = "${var.aws_region}"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}
