# Defining Region
variable "aws_region" {
  default = "eu-west-3"
}

# Defining CIDR Block for VPC
# Showing demo
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "aws_access_key" {
  description = "AWS Access Key ID"
}

variable "aws_secret_key" {
  description = "AWS Secret Access Key"
}
